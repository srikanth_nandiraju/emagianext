// Path.rescue(function () {
//     alert("No such screen");
// });

Path.map("#/login").to(function () {
    App.FullScreen.show("screens/login.html");
    $(".fullscreen-close").hide();
    //$("#stage").load("screens/login.html");
});

Path.map("#/logout").to(function () {
    App.Events.emit("user-logged-out");
    App.FullScreen.show("screens/login.html");
    $(".fullscreen-close").hide();
});

Path.map("#/profile").to(function () {
    $("#stage").load("screens/profile.html");
});

Path.map("#/organizations").to(function () {
    $("#stage").load("screens/organizations.html");
});

Path.root("#/organizations");
Path.listen();